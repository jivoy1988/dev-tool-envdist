import { StrategiesEnum } from '../enums/strategies.enum';

export interface IArguments {
    strategy: StrategiesEnum;
    environment: string;
    dist?: string;
    pattern?: string;
    resolve?: boolean;
    override?: boolean;
}
