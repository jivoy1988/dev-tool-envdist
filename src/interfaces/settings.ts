export interface ISettings {
    path?: string;
    dist?: string;
    pattern?: string;
    strategies?: IStrategies;
    ignore?: string[],
}

export interface IStrategies {
    manual?: string[],
    variables?: string[],
}
