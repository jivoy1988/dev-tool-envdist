export enum StrategiesEnum {
    Manual = 'm',
    Force = 'f',
    Variables = 'e',
}
