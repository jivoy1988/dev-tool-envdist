import { StrategiesEnum } from '../../enums';
import { AbstractStrategy, ForceStrategy, VariablesStrategy, ManualStrategy } from './strategies';
import { Generator } from './generator';
import { Console } from '../console';

export class GeneratorFactory {
    constructor(private readonly console: Console) {
    }

    create(strategyName: StrategiesEnum): Generator {
        return new Generator(this.createStrategy(strategyName));
    }

    private createStrategy(strategyName: StrategiesEnum): AbstractStrategy {
        switch (strategyName) {
            case StrategiesEnum.Force:
                return new ForceStrategy(this.console);
            case StrategiesEnum.Variables:
                return new VariablesStrategy(this.console);
            default:
                return new ManualStrategy(this.console);
        }
    }
}
