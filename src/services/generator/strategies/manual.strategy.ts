import { AbstractStrategy } from './abstract.strategy';
import { IDistVariable } from '../../../interfaces';
import { Colors } from '../../../enums';

export class ManualStrategy extends AbstractStrategy {
    async generate(variable: IDistVariable): Promise<string> {
        const message = `# ${variable.description}\n${Colors.FgYellow}${variable.name}${Colors.FgCyan}=`;
        const value = await this.console.readLine(message, variable.default);

        return `${variable.name}=${value || variable.default || ''}`.trim();
    }
}
