import { IDistVariable, IEnvVariable } from '../../interfaces';

export class EnvParser {
    private readonly countEnvGroups = 3;
    private readonly countDistGroups = 4;
    private readonly countDistGroupsWithDefault = 6;

    private readonly distRegex = /^([A-Za-z_0-9]{1,255})[\s\t]*=[\s\t]*(\<(.+)\>)([\s\t]*\#[\s\t]*(.+))?$/m;
    private readonly envRegex = /^([A-Za-z_0-9]{1,255})[\s\t]*=[\s\t]*([^\s].*)$/m;

    parseDist(line: string): IDistVariable | null {
        const match = line.match(this.distRegex);

        if (!match || (match.length !== this.countDistGroups && match.length !== this.countDistGroupsWithDefault)) {
            return null;
        }

        return {
            name: match[1],
            description: match[3],
            default: match[5],
        };
    }

    parseEnv(line: string): IEnvVariable | null {
        const match = line.match(this.envRegex);

        if (!match || (match.length !== this.countEnvGroups)) {
            return null;
        }

        return {
            name: match[1],
            value: match[2],
        }
    }
}
