import * as path from 'path';
import * as fs from 'fs';
import { ISettings } from '../../interfaces';

export class SettingsParser {
    parse(dir: string): ISettings {
        const packageJson = path.resolve(dir, 'package.json');
        if (!fs.existsSync(packageJson)) {
            return {};
        }

        const config = require(packageJson);
        if (!config.env) {
            return {};
        }

        const { dist, pattern, strategies, ignore } = config.env;
        return {
            path: config.env.path,
            dist,
            pattern,
            strategies,
            ignore,
        };
    }
}
