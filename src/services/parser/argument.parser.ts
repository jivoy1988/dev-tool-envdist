import { InvalidArgumentsException } from '../../exceptions';
import { IArguments } from '../../interfaces';
import { StrategiesEnum } from '../../enums';
import yargs from 'yargs';

const DEFAULT_STRATEGY = StrategiesEnum.Manual;

export class ArgumentParser {
    private readonly environmentRegex = /^[A-z]{1,255}$/m;
    private readonly flagRegex = /^(\-([fme]))?$/m;

    parse(args: string[]): IArguments {
        args = args.slice(2);

        const argv = yargs(args).options({
            'manual': {
                alias: '-m',
                type: 'boolean',
                default: false
            },
            'force': {
                alias: '-f',
                type: 'boolean',
                default: false
            },
            'resolve': {
                alias: '-r',
                type: 'boolean',
                default: false
            },
            'override': {
                alias: '-o',
                type: 'boolean',
                default: false
            },
            'variables': {
                alias: '-e',
                type: 'boolean',
                default: false
            },
            'dist': {
                alias: '-d',

                type: 'string',
                default: null
            },
            'pattern': {
                alias: '-p',
                type: 'string',
                default: null
            }
        }).parse(args);

        let environment = process.env['NODE_ENV'];

        if (argv._.length !== 0) {
            environment = this.parseEnvironment(argv._[0]);
        }

        if (argv._.length > 1) {
            throw new InvalidArgumentsException();
        }

        let strategy = DEFAULT_STRATEGY;

        if (argv.m) {
            strategy = StrategiesEnum.Manual;
        }

        if (argv.f) {
            strategy = StrategiesEnum.Force;
        }

        if (argv.e) {
            strategy = StrategiesEnum.Variables;
        }

        return {
            environment: environment,
            strategy: strategy,
            dist: argv.dist,
            pattern: argv.pattern,
            resolve: argv.resolve,
            override: argv.override,
        };
    }

    private fetchStrategy(data: string[]): StrategiesEnum {
        if (data[0]) {
            const first = this.parseFlag(data[0]);
            if (first) {
                data.shift();
                return first as StrategiesEnum;
            }
        }

        return DEFAULT_STRATEGY;
    }

    private fetchEnvironment(data: string[]): string {
        if (data[0]) {
            const first = this.parseEnvironment(data[0]);

            if (first) {
                data.shift();
                return first;
            }

            if (data.length <= 1) {
                return process.env['NODE_ENV'];
            }
        }

        if (data[1]) {
            const last = this.parseEnvironment(data[1]);

            if (last) {
                data.pop();
                return last;
            }
        }

        return process.env['NODE_ENV'];
    }

    private parseEnvironment(value: string): string | null {
        const match = value.match(this.environmentRegex);

        if (!match || !match.length) {
            return null;
        }

        return match[0];
    }

    private parseFlag(value: string): string | null {
        const match = value.match(this.flagRegex);

        if (!match || !match.length) {
            return null;
        }

        return match[2];
    }
}
