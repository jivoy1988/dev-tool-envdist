import * as readline from 'readline';
import { Colors } from '../enums';

export class Console {
    private process: readline.Interface;

    constructor() {
        this.process = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
    }

    writeLine(text: string, color: Colors = Colors.Reset): void {
        console.log(`${color}${text}${Colors.Reset}`);
    }

    readLine(text: string, defaultValue: string = ''): Promise<string> {
        this.process.write(defaultValue);
        return new Promise<string>(resolve => this.process.question(`${text}${Colors.Reset}`, resolve));
    }

    info(text: string): void {
        this.writeLine(text, Colors.FgGreen);
    }

    error(text: string): void {
        this.writeLine(text, Colors.FgRed);
    }

    warning(text: string): void {
        this.writeLine(text, Colors.FgYellow);
    }

    exit(): void {
        this.process.close();
    }
}
