import { AbstractCommand } from './command';
import { InvalidArgumentsException } from '../../exceptions';
import { HelpCommand } from './help.command';
import { ArgumentParser, SettingsParser,  } from '../parser';
import { ENV_VAR, GeneratorFactory } from '../generator';
import { ISettings, IArguments } from '../../interfaces';
import { StrategiesEnum } from '../../enums';

export class GenerateCommand extends AbstractCommand {
    async execute(): Promise<void> {
        try {
            const args = new ArgumentParser().parse(process.argv);
            const settings = new SettingsParser().parse(process.cwd());

            if (args.dist) {
                settings.dist = args.dist;
            }

            if (args.pattern) {
                settings.pattern = args.pattern;
            }

            await new GeneratorFactory(this.console)
                .create(this.resolveStrategy(settings, args))
                .generate(settings, args);

            this.console.info('Env file successful generated.');
            this.console.exit();
        } catch (e) {
            if (e instanceof InvalidArgumentsException) {
                return new HelpCommand().execute();
            }

            this.console.error(e.message);
            this.console.exit();
        }
    }

    private resolveStrategy(settings: ISettings, args: IArguments): StrategiesEnum {
        if (!settings.strategies) {
            return args.strategy;
        }

        const manual = settings.strategies.manual;
        const currentEnv = process.env[ENV_VAR] || '';

        if (Array.isArray(manual) && manual.includes(currentEnv)) {
            return StrategiesEnum.Manual;
        }

        const variables = settings.strategies.variables;

        if (Array.isArray(variables) && variables.includes(currentEnv)) {
            return StrategiesEnum.Variables;
        }

        return args.strategy;
    }
}
