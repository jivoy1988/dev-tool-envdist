import * as path from 'path';
import * as fs from 'fs';

export class FileManager {
    read(fullName: string): string[] {
        const content = fs.readFileSync(fullName).toString();
        return content.split('\n');
    }

    save(filePath: string, filename: string, content: string): void {
        const fullName = path.resolve(filePath, filename);
        fs.writeFileSync(fullName, content);
    }

    exist(dir: string, filename: string = ''): boolean {
        const fullName = path.resolve(dir, filename);

        return fs.existsSync(fullName)
    }
}
