#!make

VERSION=`cat VERSION`

all:
	@echo "Please choose a task."
.PHONY: all

test:
	npm install --no-progress
	npm run test
.PHONY: test

lint: lint-yaml
.PHONY: lint

lint-yaml:
	yamllint . --strict
.PHONY: lint-yaml

bumpversion:
	bumpversion patch;
.PHONY: bumpversion

release-start:
	git flow release start -F $(VERSION);
.PHONY: release-start

changelog:
	auto-changelog --ignore-commit-pattern "(Merge tag '.+' into develop|Merge branch 'release\/.+'|Bump version:|Update CHANGELOG.md)" --commit-limit false --release-summary --issue-pattern "[A-Z]+-\d+" --template keepachangelog.hbs --latest-version $(VERSION); git commit --only -m "Update CHANGELOG.md" -- CHANGELOG.md;
.PHONY: changelog

release-finish:
	export GIT_MERGE_AUTOEDIT=no; git flow release finish -F -p -m "$(VERSION)\n\n$(VERSION)" -T $(VERSION) $(VERSION); unset GIT_MERGE_AUTOEDIT;
.PHONY: release-finish

release-npm:
	npm i --no-progress
	npx tsc
	npm publish
.PHONY: release-npm

release: bumpversion release-start changelog release-finish release-npm
.PHONY: release
