import * as path from 'path';
import { SettingsParser } from '../../../src/services/parser';

const PATH = 'env';
const DIST = 'test.env.dist';
const PATTERN = '*.test.env';
const FILES_PATH = path.resolve(__dirname, '..', 'files');

describe('SettingsParser', () => {
    let settingsParser: SettingsParser;

    beforeAll(() => {
        settingsParser = new SettingsParser();
    });

    it('Method parse. The file package.json does not exists.', () => {
        const settings = settingsParser.parse(FILES_PATH);
        expect(settings).not.toBeUndefined();
        expect(settings.pattern).toBeUndefined();
        expect(settings.dist).toBeUndefined();
        expect(settings.path).toBeUndefined();
    });

    it('Method parse. Declared env configuration.', () => {
        const settings = settingsParser.parse(path.resolve(FILES_PATH, 'declared-env'));
        expect(settings).not.toBeUndefined();
        expect(settings.pattern).toBe(PATTERN);
        expect(settings.dist).toBe(DIST);
        expect(settings.path).toBe(PATH);
    });

    it('Method parse. Undeclared env configuration.', () => {
        const settings = settingsParser.parse(path.resolve(FILES_PATH, 'undeclared-env'));
        expect(settings).not.toBeUndefined();
        expect(settings.pattern).toBeUndefined();
        expect(settings.dist).toBeUndefined();
        expect(settings.path).toBeUndefined();
    });
});
