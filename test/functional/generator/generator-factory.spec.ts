import { ForceStrategy, GeneratorFactory, ManualStrategy, VariablesStrategy } from '../../../src/services/generator';
import { Console } from '../../../src/services';
import { StrategiesEnum } from '../../../src/enums';

describe('GeneratorFactory', () => {
    let generatorFactory: GeneratorFactory;

    beforeAll(() => {
        generatorFactory = new GeneratorFactory(new Console());
    });

    it('Method create. Variables strategy.', () => {
        const generator = generatorFactory.create(StrategiesEnum.Variables);
        expect(generator.getStrategy()).toBeInstanceOf(VariablesStrategy);
    });

    it('Method create. Force strategy.', () => {
        const generator = generatorFactory.create(StrategiesEnum.Force);
        expect(generator.getStrategy()).toBeInstanceOf(ForceStrategy);
    });

    it('Method create. Manual strategy.', () => {
        const generator = generatorFactory.create(StrategiesEnum.Manual);
        expect(generator.getStrategy()).toBeInstanceOf(ManualStrategy);
    });

    it('Method create. Unknown strategy.', () => {
        const generator = generatorFactory.create(undefined);
        expect(generator.getStrategy()).toBeInstanceOf(ManualStrategy);
    });
});
