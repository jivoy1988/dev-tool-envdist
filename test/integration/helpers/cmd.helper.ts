import * as child_process from 'child_process';

export function cmd(command: string, args: string[]): Promise<void> {
    return new Promise<void>((resolve, reject ) => {
        const process = child_process.spawn(command, args);
        process.on('close', code => code === 0 ? resolve() : reject(code));
    });
}
